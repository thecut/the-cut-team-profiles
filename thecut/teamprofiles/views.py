# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from .models import Member, Team
from django.views import generic


class MemberDetailView(generic.DetailView):

    model = Member

    template_name_field = 'template'

    def get_queryset(self, *args, **kwargs):
        queryset = super(MemberDetailView, self).get_queryset(*args, **kwargs)
        return queryset.current_site().active()


class TeamListView(generic.ListView):

    model = Team

    def get_queryset(self, *args, **kwargs):
        queryset = super(TeamListView, self).get_queryset(*args, **kwargs)
        return queryset.active()
