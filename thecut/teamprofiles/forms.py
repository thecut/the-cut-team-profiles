# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from .models import Member
from django import forms


class MemberAdminForm(forms.ModelForm):

    class Meta(object):
        fields = ['title', 'short_name', 'long_name', 'suffix', 'position',
                  'image', 'date_of_birth', 'gender', 'biography', 'notes',
                  'teams', 'meta_description', 'tags',
                  'site', 'slug', 'is_enabled', 'template', 'is_featured',
                  'is_indexable']
        labels = {'long_name': 'Name'}
        model = Member
        widgets = {'teams': forms.CheckboxSelectMultiple()}
