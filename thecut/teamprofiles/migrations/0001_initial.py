# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
from django.conf import settings
import thecut.teamprofiles.models
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0002_auto_20150616_2121'),
        ('contacts', '0003_set_ondelete'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('sites', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Member',
            fields=[
                ('contact_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='contacts.Contact')),
                ('title', models.CharField(max_length=250, blank=True)),
                ('short_name', models.CharField(db_index=True, max_length=250, blank=True)),
                ('long_name', models.CharField(db_index=True, max_length=250, blank=True)),
                ('suffix', models.CharField(max_length=250, blank=True)),
                ('date_of_birth', models.DateField(null=True, blank=True)),
                ('gender', models.CharField(default='', max_length=1, blank=True, choices=[('M', 'Male'), ('F', 'Female')])),
                ('order', models.PositiveIntegerField(default=0)),
                ('is_indexable', models.BooleanField(default=True, help_text='Should this page be indexed by search engines?', db_index=True, verbose_name='indexable')),
                ('meta_description', models.CharField(default='', help_text='Optional short description for use by search engines.', max_length=200, blank=True)),
                ('position', models.CharField(default='', max_length=200, blank=True)),
                ('slug', models.SlugField()),
                ('template', models.CharField(default='', help_text='Example: "app/model_detail.html".', max_length=100, blank=True)),
                ('site', models.ForeignKey(default=thecut.teamprofiles.models.get_current_site, to='sites.Site')),
            ],
            options={
                'ordering': ['order', 'pk'],
                'abstract': False,
                'get_latest_by': 'created_at',
                'verbose_name': 'team member',
            },
            bases=('contacts.contact', models.Model),
        ),
        migrations.CreateModel(
            name='SocialProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('url', models.URLField(max_length=256)),
                ('created_by', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, editable=False, to=settings.AUTH_USER_MODEL)),
                ('member', models.ForeignKey(related_name='profiles', to='teamprofiles.Member')),
            ],
        ),
        migrations.CreateModel(
            name='SocialProfileType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=256)),
                ('slug', models.SlugField(max_length=256)),
                ('created_by', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, editable=False, to=settings.AUTH_USER_MODEL)),
                ('updated_by', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, editable=False, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(db_index=True, max_length=250, blank=True)),
                ('notes', models.TextField(blank=True)),
                ('is_enabled', models.BooleanField(default=True, verbose_name='enabled')),
                ('is_featured', models.BooleanField(default=False, verbose_name='featured')),
                ('order', models.PositiveIntegerField(default=0)),
                ('slug', models.SlugField(unique=True)),
                ('created_by', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, editable=False, to=settings.AUTH_USER_MODEL)),
                ('tags', taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', blank=True, help_text='A comma-separated list of tags.', verbose_name='Tags')),
                ('updated_by', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, editable=False, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['order', 'pk'],
                'abstract': False,
                'get_latest_by': 'created_at',
            },
        ),
        migrations.AddField(
            model_name='socialprofile',
            name='profile_type',
            field=models.ForeignKey(to='teamprofiles.SocialProfileType'),
        ),
        migrations.AddField(
            model_name='socialprofile',
            name='updated_by',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, editable=False, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='member',
            name='teams',
            field=models.ManyToManyField(related_name='members', to='teamprofiles.Team', blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='socialprofile',
            unique_together=set([('profile_type', 'member')]),
        ),
        migrations.AlterUniqueTogether(
            name='member',
            unique_together=set([('site', 'slug')]),
        ),
    ]
