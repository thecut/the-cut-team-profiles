# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from .forms import MemberAdminForm
from .models import Member, SocialProfile, SocialProfileType, Team
from django.contrib import admin
from thecut.authorship.admin import AuthorshipMixin
from thecut.contacts.admin import ContactEmailInline, ContactPhoneInline
from thecut.ordering.admin import ReorderMixin


class SingleContactEmailInline(ContactEmailInline):

    max_num = 1


class SingleContactPhoneInline(ContactPhoneInline):

    max_num = 1


class SocialProfileInline(admin.TabularInline):

    model = SocialProfile

    extra = 0


@admin.register(Member)
class MemberAdmin(ReorderMixin, AuthorshipMixin, admin.ModelAdmin):

    fieldsets = [
        (None, {'fields': ['title', ('short_name', 'long_name'), 'suffix',
                           'position', 'image', 'date_of_birth', 'gender',
                           'biography', 'notes', 'teams', 'meta_description',
                           'tags']}),
        ('Publishing', {'fields': ['site', 'slug', 'is_enabled', 'template',
                                   'is_featured', 'is_indexable',
                                   ('created_at', 'created_by'),
                                   ('updated_at', 'updated_by')],
                        'classes': ['collapse']}),
    ]

    form = MemberAdminForm

    inlines = [SingleContactEmailInline, SingleContactPhoneInline,
               SocialProfileInline]

    list_display = ['name', 'position', 'is_enabled', 'is_featured',
                    'is_indexable']

    list_filter = ['teams', 'is_enabled', 'is_featured', 'is_indexable',
                   'site']

    prepopulated_fields = {'slug': ['long_name']}

    readonly_fields = ['created_at', 'created_by', 'updated_at', 'updated_by']

    search_fields = ['short_name', 'long_name', 'nicknames__value',
                     'emails__value', 'phones__value']


@admin.register(SocialProfileType)
class SocialProfileTypeAdmin(AuthorshipMixin, admin.ModelAdmin):

    fieldsets = [
        (None, {'fields': ['name']}),
        ('Publishing', {'fields': ['slug',
                                   ('created_at', 'created_by'),
                                   ('updated_at', 'updated_by')],
                        'classes': ['collapse']}),
    ]

    list_display = ['name']

    prepopulated_fields = {'slug': ['name']}

    readonly_fields = ['created_at', 'created_by', 'updated_at', 'updated_by']

    search_fields = ['name']


@admin.register(Team)
class TeamAdmin(ReorderMixin, AuthorshipMixin, admin.ModelAdmin):

    fieldsets = [
        (None, {'fields': ['name', 'notes', 'tags']}),
        ('Publishing', {'fields': ['slug', 'is_enabled', 'is_featured',
                                   ('created_at', 'created_by'),
                                   ('updated_at', 'updated_by')],
                        'classes': ['collapse']}),
    ]

    list_display = ['name']

    prepopulated_fields = {'slug': ['name']}

    readonly_fields = ['created_at', 'created_by', 'updated_at', 'updated_by']

    search_fields = ['name']
