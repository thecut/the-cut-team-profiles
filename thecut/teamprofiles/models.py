# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from . import querysets
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from thecut.authorship.models import Authorship
from thecut.contacts.models import AbstractContactGroup, AbstractPerson
from thecut.ordering.models import OrderMixin
from thecut.publishing.utils import generate_unique_slug


def get_current_site():
    try:
        return Site.objects.get_current().pk
    except Site.DoesNotExist:
        pass


class Member(OrderMixin, AbstractPerson):

    is_indexable = models.BooleanField(
        'indexable', db_index=True, default=True,
        help_text='Should this page be indexed by search engines?')

    meta_description = models.CharField(
        max_length=200, blank=True, default='',
        help_text='Optional short description for use by search engines.')

    position = models.CharField(max_length=200, blank=True, default='')

    site = models.ForeignKey('sites.Site', default=get_current_site)

    slug = models.SlugField()

    teams = models.ManyToManyField('teamprofiles.Team', related_name='members',
                                   blank=True)

    template = models.CharField(max_length=100, blank=True, default='',
                                help_text='Example: "app/model_detail.html".')

    objects = querysets.MemberQuerySet.as_manager()

    class Meta(OrderMixin.Meta, AbstractPerson.Meta):
        verbose_name = 'team member'
        unique_together = ['site', 'slug']

    def get_absolute_url(self):
        return reverse('teamprofiles:member_detail',
                       kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        if not self.slug:
            queryset = self.__class__.objects.filter(site=self.site)
            self.slug = generate_unique_slug(self.name, queryset)
        return super(Member, self).save(*args, **kwargs)


@python_2_unicode_compatible
class SocialProfileType(Authorship, models.Model):

    name = models.CharField(max_length=256)

    slug = models.SlugField(max_length=256)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class SocialProfile(Authorship, models.Model):

    url = models.URLField(max_length=256)

    profile_type = models.ForeignKey('teamprofiles.SocialProfileType')

    member = models.ForeignKey('teamprofiles.Member', related_name='profiles')

    def __str__(self):
        return '{} ({})'.format(self.profile_type, self.member)

    class Meta(object):
        unique_together = ['profile_type', 'member']


class Team(OrderMixin, AbstractContactGroup):

    slug = models.SlugField(unique=True)

    class Meta(OrderMixin.Meta, AbstractContactGroup.Meta):
        pass
