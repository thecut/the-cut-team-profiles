# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django import apps
from django.db.models import signals


class AppConfig(apps.AppConfig):

    label = 'teamprofiles'

    name = 'thecut.teamprofiles'
