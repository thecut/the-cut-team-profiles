# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from . import views
from django.conf.urls import include, url


urls = [

    url(r'^$', views.TeamListView.as_view(), name='team_list'),

    url(r'^(?P<slug>[\w-]+)$',
        views.MemberDetailView.as_view(), name='member_detail'),

]


urlpatterns = [url(r'^', include(urls, namespace='teamprofiles'))]
