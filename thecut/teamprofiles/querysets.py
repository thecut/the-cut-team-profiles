# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.contrib.sites.models import Site
from thecut.contacts.querysets import ActiveFeaturedQuerySet


class MemberQuerySet(ActiveFeaturedQuerySet):

    def current_site(self):
        site = Site.objects.get_current()
        return self.filter(site=site)

    def indexable(self):
        return self.filter(is_indexable=True).active()
