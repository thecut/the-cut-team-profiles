# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from .models import Member
from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse


class MemberSitemap(Sitemap):

    def items(self):
        return Member.objects.current_site().indexable()

    def lastmod(self, obj):
        return obj.updated_at


class TeamListSitemap(Sitemap):

    def items(self):
        return ['teamprofiles:team_list']

    def location(self, item):
        return reverse(item)


sitemaps = {'staff_member': MemberSitemap,
            'staff_team': TeamListSitemap}
